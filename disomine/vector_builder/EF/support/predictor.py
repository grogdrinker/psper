#!/usr/bin/env python
# encoding: utf-8
"""
predictor.py

Created by Elisa Cilia on 2013-03-28.
Copyright (c) 2013 Elisa Cilia. All rights reserved.
Modified and simplified for EFoldMine purposes (Wim Vranken, 07/2016)
"""

import os, glob
import subprocess
from datetime import datetime

import config
from utils import *
from pkl_utils import Pickle

class DynaMine:
    def __init__(self, ssp=False, light=False, sidechain=False, allinoneout=False, backbone=False):
        self.windowsize = 51
        #self.allw = range(3,27,2)+[51]
        # print self.allw
        self.sidechain = sidechain
        self.backbone = backbone
        self.ssp = ssp
        self.light = light
        self.allinone = allinoneout
        self.modelDict = {}
        self.modelBias = {}
        self.input = False
        self.version = '3.0'
        self.outputfilenames = []
        self.types_ = []
        self.useNewSsp = True
        if backbone:
            self.types_.append(('backbone',''))
        if ssp:
            if self.useNewSsp:
              suffix = '_new'
              self.types_.append(('ppII',suffix))
            else:
              suffix = ''
            self.types_.extend([('coil',suffix), ('sheet',suffix), ('helix',suffix)])
        if sidechain:
            self.types_.append(('sidechain',''))
        self.loadModels()
        self.adjustModels()

    def loadModels(self):
        modelLoaded = True
        for (type_,suffix) in self.types_:
            
            pickleWindowList = []
            modelDir = os.path.join(config.model_path, type_)
            pickleFileNames = glob.glob(os.path.join(modelDir,"read*.model{}.pkl.bz2".format(suffix)))
            for pfn in pickleFileNames:
              pickleWindowList.append((int(os.path.split(pfn)[1].split('.')[0][4:]),pfn))

            for (w,pklfile) in pickleWindowList:
                #print w, pklfile
                if os.path.isfile(pklfile):
                    p = Pickle()
                    model = p.load_data(pklfile)
                    modelKey = (type_, w)
                    self.modelDict[modelKey] = []
                    self.modelBias[modelKey] = model['bias']
                    # Precalculate for faster retrieval
                    for predPos in model.keys():
                        if not predPos == 'bias':
                            self.modelDict[modelKey].append({})
                            for aa in config.aas:
                                self.modelDict[modelKey][-1][aa] = 0.0
                            for (correction, aaCodes) in model[predPos]:
                                for aaCode in aaCodes:
                                    self.modelDict[modelKey][-1][aaCode] += correction
            else:
                modelLoaded = False
        return modelLoaded

    def adjustModels(self):
        """ Substitute the values for '-' with the median over the position
        """
        for modelKey, model in self.modelDict.iteritems():
            # w = len(model) / 2
            for i in range(len(model)):
                newdistribution = filterOutliers(model[i].values()) # remove outliers
                #newdistribution = model[i].values()
                m = median(newdistribution) # compute the median per position
                # print "before:", model[i]['-'], "after:", m
                model[i]['-'] = m
        
    def predictSeqs(self, seqs, jobdir, force_fileid="", force_windowsize=None, overwrite=False, writeFile=True):
        
        origdir = os.getcwd()
        count_short = 0
        idlist_ = sorted(seqs.iterkeys())
        
        if not writeFile:
          self.allPredictions = {}
        else:
          os.chdir(jobdir)

        for id_ in idlist_:
            #print 'Predicting %s...' % (id_),
            s = seqs[id_]

            if force_windowsize:
                self.windowsize = force_windowsize
            else:
              if len(s) > 51:
                  self.windowsize = 51
              elif len(s) >= 25:
                  self.windowsize = 25
              elif len(s) >= 19:
                  self.windowsize = 19
              elif len(s) == 5:
                  self.windowsize = 5
              elif len(s) < 5:
                  self.windowsize = 5
                  count_short += 1
                  #print 'skipping too short.'
                  continue
              else:
                  if len(s) % 2 == 0:
                      self.windowsize = len(s) - 1
                  else:
                      self.windowsize = len(s)

            for (predictor,suffix) in self.types_:
                preds = self.computePredictions(id_, s, predictor)

                if writeFile:
                  self.writePredictions(id_, preds, type_=predictor, force_fileid=force_fileid)
                else:
                  if id_ not in self.allPredictions.keys():
                    self.allPredictions[id_] = {}
                  self.allPredictions[id_][predictor] = preds

            #print 'done.'

        os.chdir(origdir)
        if count_short == len(seqs):
            return False
        else:
            return True

    def computePredictions(self, id_, s, type_):
        """
        Returns a list of lists (one for each amino acid in the sequence) containing two strings ['aa type one-letter code', 'prediction']
        """
        # Wim speedup attempt
        emptyTailLen = ((self.windowsize - 1) / 2)
        s_app = correctFragment("-" * emptyTailLen + "".join([x[0] for x in s]) + "-" * emptyTailLen)
        #print s_app
        modelKey = (type_, self.windowsize)
        seqLen = len(s_app)
        preds = []
        #print modelKey
        #print self.modelBias.keys()
        for seqPos in range(seqLen):
            preds.append([s_app[seqPos],self.modelBias[modelKey]])
        for seqPos in range(seqLen):
            aaCode = s_app[seqPos]
            for otherSeqPos in range(max(0,seqPos - emptyTailLen),min(seqLen,seqPos+emptyTailLen+1)):
                predPos = emptyTailLen - (otherSeqPos - seqPos)
                preds[otherSeqPos][1] += self.modelDict[modelKey][predPos][aaCode]
        return preds[emptyTailLen:-emptyTailLen]

