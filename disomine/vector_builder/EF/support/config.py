#!/usr/bin/env python
# encoding: utf-8
"""
config.py

Created by Elisa Cilia on 2013-03-28.
Copyright (c) 2013 Elisa Cilia. All rights reserved.
"""

import os
import string

aas = '-ACDEFGHIKLMNPQRSTVWYX'

base_path = os.path.abspath(os.path.dirname(__file__))

model_path = os.path.join(base_path, "models/")
