#!/usr/bin/python

"""
======================COPYRIGHT/LICENSE START==========================

efPredict.py: EFoldMine early folding prediction

Copyright (C) 2016 Wim Vranken
                   (Vrije Universiteit Brussel, Interuniversity Institute
                    of Bioinformatics in Brsusels)

=======================================================================

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

A copy of this license can be found in ../../../../license/LGPL.license

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


======================COPYRIGHT/LICENSE END============================

for further information, please contact Wim Vranken (wvranken@vub.ac.be)

=======================================================================

If you are using this software for academic purposes, please
quote the following reference:

===========================REFERENCE START=============================
D. Raimondi, G. Orlando, R. Pancsa and W.F. Vranken (2016) Sequence-based
prediction of folding initiation sites in proteins. TBD TBD

===========================REFERENCE END===============================

"""

import cPickle, os, sys, time, argparse

from support.predictor import DynaMine

class EarlyFoldPredictor:
  __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
  pickleModelFile = os.path.join(__location__ + '/models',"efModelRBF2016.cPickle")

  pickleProbabilisticModelFile = os.path.join(__location__ + '/models',"efModelRBF2016.proba.cPickle")

  def __init__(self,predType='probabilistic'):

    self.window = 2

    self.predType = predType

    #print "Reading the {} model...".format(predType)

    if predType == 'absolute':
      self.model = cPickle.load(open(self.pickleModelFile))
    elif predType == 'probabilistic':
      self.model = cPickle.load(open(self.pickleProbabilisticModelFile))

    #print "Done."

    #print "Initializing dynamine...",
    self.dynaMine = DynaMine(ssp=True,light=True,sidechain=True,allinoneout=True,backbone=True)
    #print "Done."

  def readSequences(self,fastaFile):

    print "Reading input fasta...",
    self.sequences = self.readFasta(fastaFile)
    print "Done."

  def predictEarlyFolding(self,sequence=None,dynaminePredictions=None,verbose=True):

    allPredictions = {}
    if True:
      seqId='culo'
      if len(sequence) < 7:
        raise(ValueError,"sequence is too short for ef")

      if not dynaminePredictions:
        #print seqId, sequence
        self.dynaMine.predictSeqs({seqId:sequence},"a",writeFile=False)
        dynPreds = self.dynaMine.allPredictions[seqId]
      else:
        dynPreds = dynaminePredictions[seqId]
        dynPreds['backbone'] = dynPreds['backbone'][:] # Make a copy, will be normalised, might mess up other code

      # Make sure the backbone preds are normalised!
      maxValue = max([values[1] for values in dynPreds["backbone"]])
      correction = 1 - maxValue
      for seqIndex in range(len(dynPreds["backbone"])):
        dynPreds["backbone"][seqIndex] = (dynPreds["backbone"][seqIndex][0],dynPreds["backbone"][seqIndex][1] + correction)
      x = self.buildVectors((sequence, dynPreds["backbone"],dynPreds["helix"],dynPreds["sheet"],dynPreds["coil"],dynPreds["sidechain"]), self.window)

      if self.predType == 'absolute':
        yp = self.model.decision_function(x)
      elif self.predType == 'probabilistic':
        yp = self.model.predict_proba(x)[:,1]
      return yp

  def writeEarlyFoldPreds(self,outFile,allPredictions):

    seqIds = allPredictions.keys()
    seqIds.sort()

    date = time.strftime("%Y.%m.%d_%H.%M")

    (pathName,baseName) = os.path.split(outFile)
    if pathName and not os.path.exists(pathName):
      os.makedirs(pathName)

    fout = open(outFile,'w')

    for seqId in seqIds:
      fout.write(self.getPredFileHeader().format(seqId,date))
      seqIndexes = allPredictions[seqId].keys()
      seqIndexes.sort()

      for seqIndex in seqIndexes:
        (seqLabel,predValue,predDiscrete) = allPredictions[seqId][seqIndex]
        fout.write("{}	{:7.3f}\n".format(seqLabel,predValue))

    fout.close()

  def predictAndWriteEarlyFolding(self,outFile,sequences=None,dynaminePredictions=None,verbose=True):

    allPredictions = self.predictEarlyFolding(sequences=sequences,dynaminePredictions=dynaminePredictions,verbose=verbose)

    self.writeEarlyFoldPreds(outFile,allPredictions)

  def getPredFileHeader(self):

    predFileHeader = """
*********************************************
* Predictions generated by EFpred based on  *
* DynaMine (http://dynamine.ibsquare.be)    *
*                                           *
* for {:<38s}*
* on {:<39s}*
*                                           *
* If you use these data please cite:        *
* - doi: 10.1038/ncomms3741 (2013)          *
* - doi: 10.1093/nar/gku270 (2014)          *
*********************************************
"""

    return predFileHeader

  def discretePreds(self,v):
	  if v > 0.5:
		  return 1
	  return 0

  def buildVectors(self,feats, window):
	  x = []
	  y = []
	   #          			 0		1		2		3	4		5		  6	  7		8		9			10
	  tmp = feats#db[name] = (seq, backbone, helix, sheet, coil, sidechain, s2, rsa, ground, espritzNmr, espritzXray)
	  i = 0
	  while i < len(tmp[0]):
		  #tx, ty = getSingleFeats(tmp, i)
		  # 1 backbone, 2 helix,  3 sheet,  4 coil, 5 sidechain
		  tx = self.getWindowFeats(tmp, 1, i, window)+ self.getWindowFeats(tmp, 2, i, window)+self.getWindowFeats(tmp, 3, i, window)+self.getWindowFeats(tmp, 4, i, window)+self.getWindowFeats(tmp, 5, i, window)
		  assert len(tx) == 25
		  x.append(tx)
		  i += 1
	  return x

  def getWindowFeats(self,data, featureNum, pos, w):	#feature indicates : 1 backbone, 2 helix,  3 sheet,  4 coil, 5 sidechain
	  # 1 backbone, 2 helix,  3 sheet,  4 coil, 5 sidechain
	  chosenFeat = data[featureNum][max(pos-w, 0):min(pos+w+1,len(data[1]))]
	  chosenFeatW = []
	  if pos-w < 0:
		  chosenFeatW = [-1] * -(pos-w)
	  for i in chosenFeat:
		  chosenFeatW.append(round(i[1],3))
	  if pos+w+1 > len(data[featureNum]):
		  chosenFeatW += [-1] * (pos+w+1 - len(data[featureNum]))
	  assert len(chosenFeatW) == (w*2 +1)
	  #print chosenFeatW
	  return chosenFeatW

  def readFasta(self,fileName):

    """
    Reads a FASTA file
    """

    print "Reading FASTA file {}...".format(fileName)

    fin = open(fileName)
    lines = fin.readlines()
    fin.close()

    # Quick FASTA read, handles multiple line sequences
    seqs = []
    seqId = None
    sequence = ""
    for line in lines:
      if line.startswith(">"):
        # Consolidate
        if seqId:
          seqs.append((seqId,sequence))
        cols = line.split()
        seqId = cols[0][1:].strip()
        sequence = ""
      elif line.strip():
        sequence += line.strip().replace(' ','')
    if seqId:
      # WARNING this is to get the filename out; has to match with scripts on server side!
      seqId = seqId.strip()
      seqs.append((seqId,sequence))

    return seqs

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description="EFoldMine early folding residue predictor")
  parser.add_argument('inputFile',help='Obligatory input file')
  parser.add_argument('-o',dest='outputFile',help='Output file',required=False)
  parser.add_argument('--raw',action='store_true', help='Produce raw distance from hyperplane output')

  args = parser.parse_args(sys.argv[1:])

  predType = 'probabilistic'
  if args.raw:
    predType = 'absolute'

  efp = EarlyFoldPredictor(predType)

  efp.readSequences(args.inputFile)

  preds = efp.predictEarlyFolding(verbose = False)

  if args.outputFile:

    efp.writeEarlyFoldPreds(args.outputFile,preds)

  else:

    protNames = preds.keys()
    protNames.sort()
    for protName in protNames:
      print protName
      seqCodes = preds[protName].keys()
      seqCodes.sort()
      for seqCode in seqCodes:
        print "  {:3d} {:8.2f}".format(seqCode,preds[protName][seqCode][1])
      print
