import cPickle, os, sys, time

from dynamineStandalone import DynaMine

class MineSuitePredictor:

  pickleProbabilisticModelFile = os.path.abspath(os.path.join(os.path.dirname(__file__),'models',"efModelRBF2016.proba.cPickle"))
  
  def __init__(self):
  
    self.window = 2

    print "Reading the early folding probabilistic model..."
    self.model = cPickle.load(open(self.pickleProbabilisticModelFile))
    print self.model
    print "Done."

    print "Initializing dynamine...",
    self.dynaMine = DynaMine()
    print "Done."
    
  def readSequences(self,fastaFile):
  
    print "Reading input fasta...",
    self.sequences = self.readFasta(fastaFile)
    print "Done."

  def readFasta(self,fileName):
    
    """
    Reads a FASTA file - from dynaMine.predictor.parsers import DynaMineFileParsers
    """
  
    print "Reading FASTA file {}...".format(fileName)

    # Bypassing FC; much faster to do like this...
    fin = open(fileName)
    lines = fin.readlines()
    fin.close()

    # Quick FASTA read, handles multiple line sequences
    seqs = []
    seqId = None
    sequence = ""
    for line in lines:
      if line.startswith(">"):
        # Consolidate
        if seqId:
          seqs.append((seqId,sequence))
        cols = line.split()
        seqId = cols[0][1:]
        sequence = ""
      elif line.strip():
        sequence += line.strip().replace(' ','')
    if seqId:
      # WARNING this is to get the filename out; has to match with scripts on server side!
      # Should have a function for this!! -> TODO with Elisa!
      seqId = seqId
      seqs.append((seqId,sequence))

    return seqs

  def predictMineSuite(self,sequences=None,verbose=True):

    if not sequences:
      sequences = self.sequences # Read in from FASTA

    print "Start predictions..."
    self.preds = {}
    
    for (seqId,sequence) in sequences:
    
      if len(sequence) < 7:
        print("Too short, ignoring")
        continue
        
      self.preds[seqId] = {}
      
      #
      # DynaMine predictions
      #
      
      self.dynaMine.predictSeqs({seqId: sequence},"a",writeFile=False)
      self.preds[seqId] = self.dynaMine.allPredictions[seqId]
      
      #  
      # Separate variable where the value of the DynaMine backbone preds is 1 - necessary for the early folding prediction!
      #
      
      dynaMineBackbone = self.preds[seqId]['backbone'][:] # Make a copy, will be shifted to maximum value of 1, might mess up other code
      
      maxValue = max([values[1] for values in dynaMineBackbone])
      correction = 1 - maxValue
      for seqIndex in range(len(dynaMineBackbone)):
        dynaMineBackbone[seqIndex] = (dynaMineBackbone[seqIndex][0],dynaMineBackbone[seqIndex][1] + correction)
      
      #
      # Run the early folding prediction
      #
      
      x = self.buildVectors((sequence, dynaMineBackbone,self.preds[seqId]["helix"],self.preds[seqId]["sheet"],self.preds[seqId]["coil"],self.preds[seqId]["sidechain"]), self.window)

      yp = self.model.predict_proba(x)[:,1]
      
      #
      # Add to prediction variable
      #
  
      assert len(yp) == len(sequence)    
      
      self.preds[seqId]['EFoldMine'] = {}
      
      i = 0
      if verbose:
        print ("Predictions for protein {} ***********".format(seqId))

      while i < len(sequence):
        if verbose:
          print("{}\t{:3.2f}\t{}".format(sequence[i], yp[i], self.discretePreds(yp[i])))
        
        self.preds[seqId]['EFoldMine'][i+1] = (sequence[i], yp[i], self.discretePreds(yp[i]))
        i+=1
      
      if verbose:
        print "***********************************************"

    return self.preds

  def discretePreds(self,v):
	  if v > 0.5:
		  return 1
	  return 0

  def buildVectors(self,feats, window):
	  x = []
	  y = []	
	   #          			 0		1		2		3	4		5		  6	  7		8		9			10  
	  tmp = feats#db[name] = (seq, backbone, helix, sheet, coil, sidechain, s2, rsa, ground, espritzNmr, espritzXray)
	  i = 0
	  while i < len(tmp[0]):
		  #tx, ty = getSingleFeats(tmp, i)
		  # 1 backbone, 2 helix,  3 sheet,  4 coil, 5 sidechain			
		  tx = self.getWindowFeats(tmp, 1, i, window)+ self.getWindowFeats(tmp, 2, i, window)+self.getWindowFeats(tmp, 3, i, window)+self.getWindowFeats(tmp, 4, i, window)+self.getWindowFeats(tmp, 5, i, window)	
		  assert len(tx) == 25					
		  x.append(tx)			
		  i += 1	
	  return x

  def getWindowFeats(self,data, featureNum, pos, w):	#feature indicates : 1 backbone, 2 helix,  3 sheet,  4 coil, 5 sidechain
	  # 1 backbone, 2 helix,  3 sheet,  4 coil, 5 sidechain
	  chosenFeat = data[featureNum][max(pos-w, 0):min(pos+w+1,len(data[1]))]	
	  chosenFeatW = []
	  if pos-w < 0:
		  chosenFeatW = [-1] * -(pos-w)		
	  for i in chosenFeat:
		  chosenFeatW.append(round(i[1],3))	
	  if pos+w+1 > len(data[featureNum]):
		  chosenFeatW += [-1] * (pos+w+1 - len(data[featureNum]))	
	  assert len(chosenFeatW) == (w*2 +1)	 
	  #print chosenFeatW
	  return chosenFeatW

if __name__ == '__main__':

  if len(sys.argv) < 2:
    print "\n\nToo few arguments in the command line!\n\nUSAGE: python standalone.py INPUT_FASTA_FILE\n\nNow exiting!"
  else:
    msp = MineSuitePredictor()
    msp.readSequences(sys.argv[1])

    preds = msp.predictMineSuite(verbose = False)

    protNames = preds.keys()
    protNames.sort()
    for protName in protNames:
      print protName
      predTypes = preds[protName].keys()
      predTypes.sort()
      for predType in predTypes:
        print predType, preds[protName][predType]
        print
        #print "  {:3d} {:8.2f}".format(seqCode,preds[protName][seqCode][1])
      print
