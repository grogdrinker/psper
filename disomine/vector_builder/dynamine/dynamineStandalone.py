#!/usr/bin/env python
# encoding: utf-8
"""
dynamineStandalone.py

Created by Wim Vranken on 14 october 2016, based on original dynamine code (Elisa Cilia)
Copyright (c) 2016 Wim Vranken. All rights reserved.
"""

import os, glob, bz2, numpy
import subprocess
import cPickle as pickle

from datetime import datetime

class Pickle:

    def __init__(self):
        pass

    def dump_data(self, filename, data):
        # compression 9 is default
        with bz2.BZ2File(filename, 'wb') as f:
            pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)

    def load_data(self, filename):
        with bz2.BZ2File(filename, 'rb') as f:
            data = pickle.load(f)
            return data
        return None
                
class DynaMine:
    
    aas = '-ACDEFGHIKLMNPQRSTVWYX'

    model_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "models/")

    def __init__(self,types='backbone'):
    
        self.windowsize = 51

        self.modelDict = {}
        self.modelBias = {}
        self.input = False
        self.version = '3.0'
        self.outputfilenames = []

        suffix = '_new'
        if types=='backbone' or types=='sidechain' or types=='backbone':
			self.types_=[(types,'')]
        else:
			self.types_ =[(types,suffix)]
			
       #self.types_ = [('backbone',''),('sidechain',''),('ppII',suffix),('coil',suffix), ('sheet',suffix), ('helix',suffix)]

        self.loadModels()
        self.adjustModels()

    def loadModels(self):
        modelLoaded = True
        for (type_,suffix) in self.types_:
            
            pickleWindowList = []
            modelDir = os.path.join(self.model_path, type_)
            pickleFileNames = glob.glob(os.path.join(modelDir,"read*.model{}.pkl.bz2".format(suffix)))
            for pfn in pickleFileNames:
              pickleWindowList.append((int(os.path.split(pfn)[1].split('.')[0][4:]),pfn))

            for (w,pklfile) in pickleWindowList:
                #print w, pklfile
                if os.path.isfile(pklfile):
                    p = Pickle()
                    model = p.load_data(pklfile)
                    modelKey = (type_, w)
                    self.modelDict[modelKey] = []
                    self.modelBias[modelKey] = model['bias']
                    # Precalculate for faster retrieval
                    for predPos in model.keys():
                        if not predPos == 'bias':
                            self.modelDict[modelKey].append({})
                            for aa in self.aas:
                                self.modelDict[modelKey][-1][aa] = 0.0
                            for (correction, aaCodes) in model[predPos]:
                                for aaCode in aaCodes:
                                    self.modelDict[modelKey][-1][aaCode] += correction
            else:
                modelLoaded = False
        return modelLoaded

    def adjustModels(self):
        """ Substitute the values for '-' with the median over the position
        """
        for modelKey, model in self.modelDict.iteritems():
            # w = len(model) / 2
            for i in range(len(model)):
                newdistribution = self.filterOutliers(model[i].values()) # remove outliers
                #newdistribution = model[i].values()
                m = numpy.median(newdistribution) # compute the median per position
                # print "before:", model[i]['-'], "after:", m
                model[i]['-'] = m
        
    def predictSeqs(self, seqs, force_fileid="", force_windowsize=None, overwrite=False, writeFile=True):
        
		origdir = os.getcwd()
		count_short = 0
		self.allPredictions = {}
		s = seqs

		if force_windowsize:
			self.windowsize = force_windowsize
		else:
		  if len(s) > 51:
			  self.windowsize = 51
		  elif len(s) >= 25:
			  self.windowsize = 25
		  elif len(s) >= 19:
			  self.windowsize = 19
		  elif len(s) == 5:
			  self.windowsize = 5
		  elif len(s) < 5:
			  self.windowsize = 5
			  count_short += 1
			  return False
		  else:
			  if len(s) % 2 == 0:
				  self.windowsize = len(s) - 1
			  else:
				  self.windowsize = len(s)

		preds = self.computePredictions('2', s, self.types_[0][0])
		a=[row[1] for row in preds]
		return a

    def getPredictions(self, id_, s, type_='backbone'):
        """
        Returns a list of lists (one for each amino acid in the sequence) containing two strings ['aa type one-letter code', 'prediction']
        """
        predictions = readPredictions('predictions_%s_%s.txt' % (id_, type_))
        preds = getinputsegments_preds(id_, s, self.windowsize, predictions)
        return preds

    def computePredictions(self, id_, s, type_):
        """
        Returns a list of lists (one for each amino acid in the sequence) containing two strings ['aa type one-letter code', 'prediction']
        """
        # Wim speedup attempt
        emptyTailLen = ((self.windowsize - 1) / 2)
        s_app = self.correctFragment("-" * emptyTailLen + "".join([x[0] for x in s]) + "-" * emptyTailLen)
        #print s_app
        modelKey = (type_, self.windowsize)
        seqLen = len(s_app)
        preds = []
        #print modelKey
        #print self.modelBias.keys()
        for seqPos in range(seqLen):
            preds.append([s_app[seqPos],self.modelBias[modelKey]])
        for seqPos in range(seqLen):
            aaCode = s_app[seqPos]
            for otherSeqPos in range(max(0,seqPos - emptyTailLen),min(seqLen,seqPos+emptyTailLen+1)):
                predPos = emptyTailLen - (otherSeqPos - seqPos)
                preds[otherSeqPos][1] += self.modelDict[modelKey][predPos][aaCode]
        return preds[emptyTailLen:-emptyTailLen]

    
    #
    # Functions ripped from other scripts in original dynaMine distr
    #

    def correctFragment(self,frag):
        seq = ''
        for j in frag:
            if not j[0] in self.aas:
                seq += 'X'
            else:
                seq += j[0]
        return seq

    def filterOutliers(self,values):
        q3 = numpy.percentile(values, 75, interpolation='higher')
        q1 = numpy.percentile(values, 25, interpolation='lower')
        iqr = q3 - q1
        newdistribution = [v for v in values if v >= q1 - 1.5 * iqr and v <= q3 + 1.5 * iqr]
        return newdistribution
