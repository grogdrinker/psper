## Directory information ##

This directory contains high-level scripts related to DisoMine. The directories here are:

* website/            Contains the Django-related code
* vector_builder/       Code to create the features for DisoMine
* input_files_examples/    Examples of FASTA input files
* datasets/            Datasets used for DisoMine
* ASSESSEMENT/        New DISPROT data for performance assessement?

** TODO: All DisoMine code should move one level down into a separate directory **