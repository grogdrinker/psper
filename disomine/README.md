# Disomine #

## System Dependencies ##
* python 2.7.12
* tcsh (for backend)
* redis-server (for python rq queue system)

## Python Dependencies ##
* django==1.11.7
* djangorestframework==3.7.3
* jsonfield==2.0.2
* numpy==1.13.3
* requests==2.18.4
* rq==0.9.2
* scikit-learn==0.19.1
* scipy==1.0.0
* torch==0.2.0.post3
* torchvision==0.1.9
* pygments==2.2.0

You can use pip.
## How to run the server (Production)? ##

* In '~/disomine/disomine' directory
```bash
$  screen #enter screen
$  source ./disEnv/bin/activate # enter virtualenv
$  rq worker # launch queue manager
```
exit screen.

* In '~/disomine/' directory
```bash
$  sudo uwsgi --ini website_uwsgi.ini
```

Open http://bio2byte.ibsquare.be/disomine/ on your browser.


## How to run the server (test/local)? ##

* In 'disomine/' directory
```bash
$  rq worker
```

* In 'disomine/website/' directory
```bash
$  python manage.py runserver
```

Open 127.0.0.1:8000 on your browser.


## API DOC ##

#### Send request
- URL : '/api/'
- METHOD : | POST |
- DATA FORMAT : "{'PROTEINID1': 'SEQUENCE1', 'PROTEINID2': 'SEQUENCE2', '...': '....'}"
- SUCCESS RESPONSE :
  Code = 202 Accepted

		{
			"Location": "/api/queue/queueID/",
			"message": "We are processing your request, check queue to be updated about the processing status",
			"queue_id": queueID
		}

- ERROR RESPONSE: Code = 400 Bad Request, Content = {}
- NOTES : From this request you will get a queue id location to check the proessing status

#### Get queue status
- URL : '/api/queue/ID'
- METHOD : | GET |
- SUCCESS RESPONSE :

	Code = 202 Accepted

		{
			"id": queueID,
			"Location": "not available",
			"request_text": "We are processing your request, there are still N process in queue",
		}

	OR

	Code = 303 See other

		{'Location': '/api/resID'"}

- ERROR RESPONSE: Code = 404 Not found, Content = {}
- NOTES : From this resource you will get infos about the status of your request


#### Get results
- URL : '/api/ID'
- METHOD : | GET |
- SUCCESS RESPONSE :
  	Code = 200 Ok

		{
			"creation_date": "yyyy-mm-ddTHH::MM.millisecZ",
			"id": ID,
			"results": [
				{
					"disomine": [
					val1, val2, val3, .., ..],
					"proteinID": "proteinid1",
					"sequence": "sequence1"
				}
			]
		}

- ERROR RESPONSE: Code = Not found, Content = {}
- NOTES : From this resource you will get your results in JSON format



### API CALL EXAMPLE ###
Api call example using httpie ($ apt install httpie)

- Send request to webserver

		http POST http://ibsquare.be/disomine/api/ < path/input_file.json


- Response example

		{
			"Location": "/api/queue/299/",
			"message": "We are processing your request, check queue to be updated about the processing status",
			"queue_id": 299
		}


- Check queue request status

		http --follow GET http://ibsquare.be/disomine/api/queue/299

- Responses example

	- Case 1: Results still not available

				{
					"id": 299
					"location": "not available",
					"request_text": "We are processing your request, there are still 2 processes in queue",
				}

	(You have to call the server again to check if your results are available)


	- Case 2: Results available

			{
				"id": 300,
				"location": "/api/265/",
				"request_text": "Results available at /api/265/",
				"result_id": 265
			}


	(If you use --follow option in httpie you will be automatically redirected to results.)


- Get your results

		http --follow GET http://ibsquare.be/disomine/api/queue/299


- Result response example

		{
		"creation_date": "2018-01-03T13:48:23.291721Z",
		"id": 265,
		"results": [
			{
				"disomine": [
					0.4611470401287079,
					0.4550575315952301,
					...
				],
				"proteinID": "P01139",
				"sequence": "MSMLFYTLITAFLIGVQ...."
			},
			{
				"disomine": [
					0.43476158380508423,
					0.4361424148082733,
					....
				],
				"proteinID": "P67966",
				"sequence": "MPNWGGGKKCGVCQKAV..."
			}
		]
		}
## DIRECTORIES CONTENT ##

# backend #
- ASSESSMENT: the benchmark dataset downloaded from http://www.disprot.org/assessment
- datasets: the datasets coming used to train and test Espritz
- input_files_examples: some fasta and json file for debugging
- vector_builder: this folder contains all the scripts and programs that are used to build the features vectors. It also contains dynamine and psipred. Please note that it contains stuff we could be not allowd to distribute (i.e. psipred binaries)
- gru80AUC_prova.py: the python class of the NN
- train_script.py: the script that runs the train on the selected dataset and test the model on the ASSESSMENT dataset. It is a prototype but it works
- standalone.py: well organized and structured final script for predictions. Please note it needs a model trained wit htrain_script.py in order to work
- utils.py: parsers and useful scripts
- gru80_final.mtorch: the serialized final model. it is trained on both disprot-based datasets (the new and the old one). In order to load it, use torch's load function (torch.load('gru80_final.mtorch'))

