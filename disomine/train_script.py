#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  single_predictions.py
#  
#  Copyright 2017 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from utils import read_dataset_espriz,leggi_db_tosatto,SOTA_performances,leggifasta,getScoresSVR
from vector_builder.vettore_gen import build_vector
from torch_NN_gru_80AUC_prova import nn_pytorch
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score
#from modelliNNgabriele import *
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score,make_scorer,matthews_corrcoef
import cPickle as pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVR
import numpy as np
import numpy as np
import marshal, os, sys, gc
def scorer(estimator, X, y):
	ypred=estimator.decision_function(X)
	return roc_auc_score(y,ypred)
def tutto():
	print 'culo'
	X,Y=pickle.load(open('xy.m','r'))
	#X=X[:30]+X[-2030:-2000]
	#Y=Y[:30]+Y[-2030:-2000]
	print 'comincio cv'
	#X=[[0,1],[0,0.5],[0.5,1],[0,0.5],[1,1],[1,0.5],[0.5,1],[0.5,1],[0,0.2],[0,0.1]]
	#Y=[1,1,1,0,0,1,0,1,0,1]
	diz={}
	controdiz={}
	for ep in [50]:#[3,5,10,50,100,1000]:
		for nod in [50]:#[5,10,30,50,100,200]:
			print 'comincio',ep,nod
			model=lstmCRF_pytorch()
			X=np.array(X)
			Y=np.array(Y)
			a=cross_val_predict(model, X, y=Y, cv=5, n_jobs=1, verbose=0,method='decision_function')[:,1]
			pickle.dump(a,open('highway.m','w'))
			model=nn1()
			b=cross_val_predict(model, X, y=Y,cv=5, n_jobs=1, verbose=0,method='decision_function')[:,1]
			pickle.dump(b,open('nn.m','w'))
			print 'pearson',pearsonr(a,b)
			score=np.mean(a)
			diz[(ep,nod)]=score
			controdiz[score]=(ep,nod)
			sen, spe, acc, bac, inf, pre, mcc,auc=U.getScoresSVR(a, b, np.median(a))
			print sen, spe, acc, bac, inf, pre, mcc,auc

	#pickle.dump(diz,open('res_nn_grid.m','w'))
	s=sorted(controdiz.keys())
	for i in s:
		print controdiz[i],i
	#print a
def tutto_grezzo(typ,ws,epoche=500):
	print 'loading'
	train,test=read_dataset_espriz('datasets/disprot')
	train.update(test)
	#trainnmr,testnmr=read_dataset_espriz('datasets/nmr')
	#train.update(trainnmr)
	#train.update(testnmr)
	#trainnmr,testnmr=read_dataset_espriz('datasets/pdb')
	#train.update(trainnmr)
	#train.update(testnmr)
	test=leggi_db_tosatto()#SOTA_performances()
	X=[]
	Y=[]
	Xt=[]
	Yt=[]
	TEST=False
	print 'building vectors'
	for i in train.keys()[:]:
		vet=build_vector(train[i][0],typ,ws,nomeseq=i)
		X+=[np.array(vet)]
		Y+=[np.array(train[i][1])]
	if TEST:
		for i in test.keys()[:]:
			vet=build_vector(test[i][0],typ,ws,nomeseq=i)
			Xt+=[np.array(vet)]
			Yt+=[np.array(test[i][1])]
			print 'test: proteine:',len(Xt),'features',len(Xt[0][0])
	
	print 'train: proteine:',len(X),'features',len(X[0][0])
	
	model=nn_pytorch(epoche=epoche,name='ws_'+str(ws))
	model.fit(X[:],Y[:],verbose=2)
	if TEST:
		yp=model.decision_function(Xt)
		sen, spe, acc, bac, inf, pre, mcc,auc=U.getScoresSVR(np.hstack(yp), np.hstack(Yt), np.median(np.hstack(yp)))
		print 'predicting test',epoche
		print 
		print '################################################################################à'
		print sen, spe, acc, bac, inf, pre, mcc,auc
		print '################################################################################à'
		print 'predicting train'
		yp=model.decision_function(X)
		sen, spe, acc, bac, inf, pre, mcc,auc=U.getScoresSVR(np.hstack(yp), np.hstack(Y), np.median(np.hstack(yp)))
		print 
		print '################################################################################à'
		print sen, spe, acc, bac, inf, pre, mcc,auc
		print '################################################################################à'
	
def main(args):
	tutto_grezzo(typ='dyna_back,psipred,ef,dyna_side',ws=4,epoche=480)
	#tutto_grezzo(epoche=200)
	#tutto_grezzo(epoche=250)
	#tutto_grezzo(typ='dyna_back,psipred,ef,dyna_side',ws=3,epoche=1000)
	#tutto_grezzo(typ='dyna_back,psipred,ef,dyna_side',ws=4,epoche=1000)
	#tutto_grezzo(typ='dyna_back,psipred,ef,dyna_side',ws=5,epoche=1000)
	#tutto_grezzo(typ='dyna_back,psipred,ef,dyna_side',ws=8,epoche=1000)
if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
