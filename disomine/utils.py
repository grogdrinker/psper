#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  utils_general.py
#  
#  Copyright 2016 Gabriele Orlando <orlando.gabriele89@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os,re
import numpy as np

blastbin='/home/gabriele/Downloads/pconsc_depend/blast-2.2.26/bin/blastclust '
iupredbinfold='/home/gabriele/iupred/'
def getScoresSVR(pred, real, threshold):
	import math
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	while i < len(real):
		if float(pred[i])<=threshold and (real[i]==0):
			confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
		if float(pred[i])<=threshold and real[i]==1:
			confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
		if float(pred[i])>=threshold and real[i]==1:
			confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
		if float(pred[i])>=threshold and real[i]==0:
			confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
		i += 1
	#print "--------------------------------------------"
	#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
	#print "          | SSBOND            | FREE             |"
	#print "predBond  | TP: %d (%2.2f%%)  | FP: %d (%2.2f%%) |" % (confusionMatrix["TP"], (confusionMatrix["TP"]/float(confusionMatrix["TP"]+confusionMatrix["FN"]))*100, confusionMatrix["FP"], (confusionMatrix["FP"]/float(confusionMatrix["FP"]+confusionMatrix["TN"]))*100 )
	#print "predFree  | FN: %d (%2.2f%%)  | TN: %d (%2.2f%%) |" % (confusionMatrix["FN"],(confusionMatrix["FN"]/float(confusionMatrix["TP"]+confusionMatrix["FN"]))*100, confusionMatrix["TN"], (confusionMatrix["TN"]/float(confusionMatrix["FP"]+confusionMatrix["TN"]))*100)
	sen = (confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))
	spe = (confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))
	acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/float((sum(confusionMatrix.values())))
	bac = (0.5*((confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))))
	inf =((confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)
	pre =(confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FP"])))
	mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"])) )  
	from sklearn.metrics import roc_auc_score
	aucScore = roc_auc_score(real, pred)
	#print "\nSen = %3.3f" % sen
	#print "Spe = %3.3f" %  spe
	#print "Acc = %3.3f " % acc
	#print "Bac = %3.3f" %  bac
	#print "Inf = %3.3f" % inf
	#print "Pre = %3.3f" %  pre
	#print "MCC = %3.3f" % mcc
	#print "AUC = %3.3f" % aucScore
	#print "--------------------------------------------"
	return (sen, spe, acc, bac, inf, pre, mcc,aucScore)

def leggifasta(database): #legge un file fasta e lo converte in un dizionario
		f=open(database)
		uniprot=f.readlines()
		f.close()
		dizio={}
		for i in uniprot:
			#c=re.match(">(\w+)",i)  4
		
			if i[0]=='>':
					if '|'==i[3] and  '|'==i[10]:
						uniprotid=i.strip('>\n').split('|')[1]
					else:
						uniprotid=i.strip('>\n')
					dizio[uniprotid]=''
			else:
				dizio[uniprotid]=dizio[uniprotid]+i.strip('\n')
		return dizio
def read_dataset_espriz(dire='datasets/nmr'):
	train={}
	#seq=''
	salto=0
	for i in os.listdir(dire+'/'+'train'):
		if i.split('.')[1]=='target':
			continue
		seq=open(dire+'/train/'+i).readlines()[1].strip()
		name=i.split('.')[0]
		true=list(open(dire+'/train/'+name+'.target').readlines()[0].strip())
		for k in range(len(true)):
			if true[k]=='X':
				print 'in',i,'X converted to 0'
				true[k]=0
				continue
			true[k]=int(true[k])
		
		if len(seq)==len(true):
			train[name]=[seq,true]
		else:
			#print 'salto' ,name
			assert not 1 in true
			true=true[1:]
			train[name]=[seq,true]
	test={}
	saltot=0
	for i in os.listdir(dire+'/'+'test'):
		if i.split('.')[1]=='target':
			continue
		seq=open(dire+'/test/'+i).readlines()[1].strip()
		name=i.split('.')[0]
		true=list(open(dire+'/test/'+name+'.target').readlines()[0].strip())
		for k in range(len(true)):
			if true[k]=='X':
				print 'in',i,'X converted to 0'
				true[k]=0
				continue
			true[k]=int(true[k])
	
		if len(seq)==len(true):
			test[name]=[seq,true]
		else:
			saltot+=1
			assert not 1 in true
			true=true[1:]
	print 'skipped Train',salto,'skipped_test',saltot
	return train,test	
def run_iupred(seqs):
	######################
	###   	MUST BE RUN FROM NORMAL TERMINAL ( NO F5)
	###     FIRST YOU HAVE TO EXPORT IUPred_PATH=iupredfolder
	###
	#################
	#os.system('export IUPred_PATH='+iupredbinfold)
	if type(seqs)==str:
		seqs=leggifasta(seqs)
	pred={}
	for i in seqs.keys():
		
		f=open('tmp.fasta','w')
		f.write('>'+i+'\n'+seqs[i]+'\n')
		f.close()
		os.system(iupredbinfold+'iupred tmp.fasta long > tmp.out')
		f=open('tmp.out','r')
		pred[i]=[]
		for j in f:
			if j[0]=='#' or j[0]=='\n':
				continue
			
			q=re.match('\s+\d+\s+\w+\s+(\d+\.\d+)',j)
			pred[i]+=[float(q.group(1))]
	return pred
def make_blastclust(f,folder=False,scelta='primo'):
	def parse_clus(fil,seqs,scelta):
		buone=[]
		if scelta=='primo':
			for i in open(fil,'r'):
				i=i.split(' ')
				buone+=[i[0]]
		elif scelta=='piu_lunga':
			for i in open(fil,'r'):
				i=i.strip().split(' ')
				lon=0
				
				for j in i:
					if len(seqs[j])>lon:
						lon=len(seqs[j])
						best=j
				buone+=[j]
		return buone
	def remove_short_sequences(fil):
		a=leggifasta(fil)
		f=open('PDBf.fasta','w')
		for i in a.keys():
			if len(a[i])>50:
				f.write('>'+i+'\n'+a[i]+'\n')
	def remove_non_unique_IDS(fil):
		a=leggifasta(fil)
		b={}
		seqs={}
		for i in a.keys():
			if not seqs.has_key(a[i]):
				if not 'XXXXXXXX' in a[i] or 'UUUUUUU' in a[i] or 'BBBBBBBB' in a[i]:
					b[i.split('|')[0]]=a[i]
				seqs[a[i]]=1
		g=open('PDBf.fasta','w')
		for i in b.keys():
			g.write('>'+i+'\n'+b[i]+'\n')
	def blastclust(fil):
		os.system(blastbin+'-i '+fil+' -o '+fil.replace('.fasta','.clusters')+' -S 20')
		diz=leggifasta(fil)
		buone=parse_clus(fil.replace('.fasta','.clusters'),diz,scelta=scelta)
		f=open(fil.replace('.fasta','')+'_blastclust.fasta','w')
		#print buone
		for i in diz.keys():
			if not i in buone:
				continue
			else:
				f.write('>'+i+'\n'+diz[i]+'\n')
	if folder==False:
		blastclust(f)
	else:
		for i in os.listdir(f):
			if not i.split('.')[-1]=='fasta':
				continue
			print 'faccio ',i
			blastclust(f+i)
def leggi_db_tosatto(fil='ASSESSMENT/disprot7_complement.fasta'):
	cont=0
	diz={}
	for i in open(fil).readlines():
		if cont==0:
			nome=i.strip()[1:]
			dis=[]
			seq=''
		elif cont%3==0:
			diz[nome]=(seq,dis)
			nome=i.strip()[1:]
			dis=[]
			seq=''
		elif cont%3==1:
			seq=i.strip()
		elif cont%3==2:
			dis=list(i.strip())	
			for j in range(len(dis)):
				dis[j]=int(dis[j])
			
		cont+=1
	diz[nome]=(seq,dis)
	return diz	
def SOTA_performances(json_file='ASSESSMENT/disprot-IDpredictions.multijson'):
	import json
	diz={}
	with open(json_file) as data_file:
		for i in data_file:
			data = json.loads(i)
			for pred in range(len(data['predictions'])):
				prot_id= data['id']
				
				predictor=data['predictions'][pred]['predictor']
				if not diz.has_key(predictor):
					diz[predictor]={}
				diz[predictor][prot_id]=data['predictions'][pred]['scores']
	db=leggi_db_tosatto()
	buone={}
	del diz[u'glo']
	for i in db.keys():
		male=False
		for predittore in diz.keys():
			if not diz[predittore].has_key(i):
				male=True
		if not male:
			buone[i]=db[i]
	print len(db),len(buone)
	for predittore in diz.keys():
		yp=[]
		Yt=[]
		for prot in buone.keys():
			yp+=diz[predittore][prot]
			Yt+=buone[prot][1]
		sen, spe, acc, bac, inf, pre, mcc,auc=U.getScoresSVR(yp,Yt, 0.5)
		print predittore,auc,mcc
	f=open('buone.fasta','w')
	for i in buone.keys():
		f.write('>'+i+'\n'+buone[i][0]+'\n')
	return buone
def main(args):
	#make_blastclust('/home/gabriele/HD1/rigapollo/datasets/idps_tuo/',True)
	#seqs={'ugo':'AAAAAAAAAAAAACCCCCCCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACC'}
	#seqs='/home/gabriele/HD1/biascp/disorder/STRUCT.fasta'
	#SOTA_performances()
	db=leggi_db_tosatto()
	print db
	#a=leggi_db_tosatto()
	#print a
	#a=read_dataset_espriz('datasets/pdb/')
	#if a[0].has_key('2w73') or a[1].has_key(''):
	#	print True
	'''
	for i in a[0].keys():
		
		if '2W73' in i:
			print True
	for i in a[1].keys():
		if '2W73' in i:
			print True
	'''
	#a=run_iupred(seqs)
	#marshal.dump(a,open('STRUCTiupred.m','w'))
	#fil='/home/gabriele/HD1/biascp/disorder/NOSTRUCT.fasta'
	#make_blastclust(fil,folder=False,scelta='piu_lunga')
	#for i in 
	return 0

if __name__ == '__main__':
	
    import sys,marshal
    sys.exit(main(sys.argv))
