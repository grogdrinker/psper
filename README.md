# PSPer

## Dependencies:
### python packages (you can install them from pip)

###### python2 
###### pytorch
###### sklearn
###### numpy
###### scipy

### Other Dependencies: 

###### csh --> from any linux official repository
###### HMMer 3.1--> from any linux official repository
###### PSIPRED single sequence:

	1. Download it from http://bioinfadmin.cs.ucl.ac.uk/downloads/psipred/ (the tar.gz file) and place the "psipred" folder in psper/disomine/vector_builder/.

	To do so you can use the following commands
	- go into the psper folder (root of the entire repo)
	$	cd psper/disomine/vector_builder/
	$   wget bioinfadmin.cs.ucl.ac.uk/downloads/psipred/psipred.4.02.tar.gz
	$   tar -xvf psipred.4.02.tar.gz
	
	2. Open the psper/disomine/vector_builder/psipred/runpsipred_single file with a text editor and modify lines 13 and 16 in the following way:
	
		set execdir = disomine/vector_builder/psipred/bin
		set datadir = disomine/vector_builder/psipred/data

## Usage

run "python PSPer.py -h" for the list of options
run "python PSPer.py -i input_example.fasta --viterbi -o output_file" to run both the evaluation and the viterbi on the examples
